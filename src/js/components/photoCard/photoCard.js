import React from 'react';
import ReactDOM from 'react-dom';
import { EventEmitter } from '../eventEmitter/eventEmitter';

export default class Car extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            brand: "Ford",
            model: "Mustang",
            color: "red",
            year: 1964,
            photo: null
        };
    }
    // used to update state from props on load
    static getDerivedStateFromProps(props, state) {
        return {photo: props.photo }; // sets the color property on state object to the prop favcol
    }
    // calls function when the dom has rendered
    componentDidMount() {

    }
    render() {
        return (
                <section className="photo-card"  onClick={(event) => EventEmitter.dispatch('photoSelected', this.state.photo)}>
                <img className="photo-card__img" src={this.props.flickerService.getPhotoUrlFromFlickerObject(this.state.photo)} alt=""/>
                    {this.props.viewMode === false &&
                    <section className="photo-card__details">
                        <h3 className="photo-card__details__title">
                            {this.state.photo.title.slice(0, 50) || 'No title'}
                        </h3>
                    </section>
                    }

                    {this.props.viewMode &&
                    < section className="photo-card__details">
                        <h3 className="photo-card__details__author" title="photo.title">
                        <a target="_blank" href={'https://www.flickr.com/photos/' + this.state.photo.owner.nsid}>
                        <i className="fa fa-user"></i>
                    {this.state.photo.owner.username}
                        </a>
                        </h3>

                        <h3 className="photo-card__details__title">
                        <a target="_blank" href={this.state.photo.urls.url[0]._content}>{this.state.photo.title._content || 'No title'}</a>
                        </h3>

                        <p className="photo-card__details__desc">{this.state.photo.description._content || 'No description'}</p>

                        {this.state.photo.tags.tag.length > 0 ? (
                            <div className="photo-card__details__tags">Tags:
                                {this.state.photo.tags.tag.map(tag => (
                                    <span key={tag.id}> {tag._content}</span>
                                ))}

                            </div>
                        ) : (
                            <p className="photo-card__details__tags">No tags</p>
                        )
                        }
                        </section>
                    }


    </section>
        );
    }
}

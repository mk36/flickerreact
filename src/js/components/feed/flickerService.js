import axios from 'axios';
export default class flickerService{

    constructor(){

    }

    /**
     * Get the flciker feed
     * @returns {Promise<unknown>}
     */
    getFlickerFeed(query){
        return new Promise(function (resolve, reject) {
            let url = 'https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=05d20d680f2e72d588a1bd65b060dddb&format=json&nojsoncallback=1&parse_tags=1&safe_search=1';

            query += '&per_page=30';
            url += query;

           axios.get(url).then(function (result) {
               resolve(result.data.photos);
           }).catch(function (error) {
               reject(error);
           });
        })
    }

    /**
     * Get details of a photo from flickr
     * @param photoId
     * @returns {Promise<unknown>}
     */
    getPhotoDetails(photoId){
        return new Promise(function (resolve, reject) {
            let url = 'https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=05d20d680f2e72d588a1bd65b060dddb&photo_id=' + photoId + '&format=json&nojsoncallback=1&parse_tags=1';

            axios.get(url).then(function (result) {
                resolve(result.data.photo);
            }).catch(function (error) {
                reject(error);
            });
        })
    }

    // /**
    //  * Get the photo url from the flciker photo object
    //  * @param photo
    //  * @returns {string}
    //  */
    getPhotoUrlFromFlickerObject(photo){
        return 'https://farm' + photo.farm + '.staticflickr.com/' + photo.server + '/' + photo.id + '_' + photo.secret +'.jpg';
    }
}

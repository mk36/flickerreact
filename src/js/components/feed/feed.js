import React from 'react';
import ReactDOM from 'react-dom';
import FlickerService from './flickerService';
import PhotoCard from '../photoCard/photoCard';
import { EventEmitter } from '../eventEmitter/eventEmitter';

export default class Feed extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            flickerService: new FlickerService(),
            pagination: null,
            flickerResults: [],
            activePhoto: null,
            preloader: false
        };

        EventEmitter.subscribe('photoSelected', (event) => this.getPhotoDetails(event));
    }
    // calls function when the dom has rendered
    componentDidMount() {
        let vm = this;

        vm.refreshFlickerFeed();
        vm.setUpInfiniteScroll();
    }
    setUpInfiniteScroll = () => {
        let vm = this;
        let d = document.documentElement;

        window.onscroll = function() {
            let offset = d.scrollTop + window.innerHeight;
            let height = d.offsetHeight;

            if (offset > (height - 50) && !vm.state.preloader && vm.state.pagination.page < vm.state.pagination.pages) {
                console.log(parseInt(vm.state.pagination.page), parseInt(vm.state.pagination.page) + 1);
                vm.refreshFlickerFeed('&page=' + (parseInt(vm.state.pagination.page) + 1));
            }
        };
    }
    /**
     * Get the next flicker feed data
     ** */
    refreshFlickerFeed = (query = '') => {
        let vm = this;

        // toggle preloader flag whilst waiting (used for debounce and spinners)
        this.setState({preloader: true});

        vm.state.flickerService.getFlickerFeed(query).then(function (result) {
            vm.setState({pagination: result});
            vm.setState({flickerResults: [...vm.state.flickerResults, ...result.photo]});
        }).catch(function (error) {
            console.log('error', error);
        }).finally(function () {
           vm.setState({preloader: false});
        });
    }
    getPhotoDetails = (photo) => {
        let vm = this;

        vm.state.flickerService.getPhotoDetails(photo.id).then(function (result) {
            vm.setActivePhoto(result);
        }).catch(function (error) {
            console.log('could not get photo', error);
        });
    }
    /**
     * Set the active photo to display to the user
     * */
    setActivePhoto = (photo) =>{
        this.setState({activePhoto: photo});
    }
    render() {
        return (
            <div className="flicker-feed">
                <h2 className="flicker-feed__title">Flickr Photos</h2>
                <div className="flicker-feed__photos">
                {this.state.flickerResults.map((photo, index) => (
                        <PhotoCard key={index} viewMode={false} photo={photo} flickerService={this.state.flickerService} />
                ))}
                </div>

        <div className={'modal ' + (this.state.activePhoto ? 'open': '')} onClick={() => this.setActivePhoto(null)}>
            <div className="modal__content">
                {this.state.activePhoto &&
                <PhotoCard viewMode={true} photo={this.state.activePhoto} flickerService={this.state.flickerService}/>
                }
            </div>
        </div>
    </div>);
    }
}
